CPP = g++
MPICC = mpic++
BIN = bin
INC = include
OBJ = obj
SRC = src
BASEDIR = ../..
NINC = $(BASEDIR)/include

# Can be changed to nida or another neuromorphic model as needed.
# I.e. make model=nida

model = danna
NLIB = $(BASEDIR)/models/$(model)/lib/libneuro.a
NEO = $(BASEDIR)/models/$(model)/eo/lib/libneuroEO.a

EO = $(BASEDIR)/eo
EOOBJ = $(EO)/obj/main.o $(EO)/obj/NeuroEO.o
DEOOBJ = $(EO)/obj/main-deo.o $(EO)/obj/NeuroEO.o $(EO)/lib/NeuroDEO.a

CFLAGSBASE = -g -Wall -Wextra -I$(INC) -I$(NINC) -pthread
CFLAGSNEURO = -I$(NINC)

LFLAGS = $(BASEDIR)/lib/libneuro.a $(NLIB)

#all: $(BIN)/TestAE $(BIN)/MNISTAP $(BIN)/TestNL $(BIN)/MNISTNP $(BIN)/MNISTEO
basic: $(BIN)/MNISTAP $(BIN)/User
eo: $(BIN)/MNISTEO
all: basic eo

clean:
	rm -f $(BIN)/* $(OBJ)/*

#$(BIN)/TestAE: $(SRC)/TestAE.cpp $(OBJ)/MNISTAE.o $(INC)/MNISTAE.hpp
#	$(CPP) $(CFLAGSBASE) $(SRC)/TestAE.cpp $(OBJ)/MNISTAE.o -o $(BIN)/TestAE

$(OBJ)/MNISTNLib.o: $(SRC)/MNISTNLib.cpp $(INC)/MNISTNLib.hpp $(BASEDIR)/lib/libneuro.a
	$(CPP) $(CFLAGSBASE) $(SRC)/MNISTNLib.cpp -c -o $(OBJ)/MNISTNLib.o

#$(BIN)/MNISTAP: $(SRC)/MNISTAP.cpp $(OBJ)/MNISTAE.o $(INC)/MNISTAE.hpp
#	$(CPP) $(CFLAGSBASE) $(SRC)/MNISTAP.cpp $(OBJ)/MNISTAE.o $(BASEDIR)/lib/libneuro.a -o $(BIN)/MNISTAP

#$(BIN)/MNISTNP: $(SRC)/MNISTNP.cpp $(OBJ)/MNISTNLib.o $(NLIB) $(BASEDIR)/lib/libneuro.a
#	$(CPP) $(CFLAGSBASE) $(CFLAGSNEURO) $(SRC)/MNISTNP.cpp $(OBJ)/MNISTNLib.o -o $(BIN)/MNISTNP $(LFLAGS)

#$(BIN)/TestBatch: $(SRC)/TestBatch.cpp $(OBJ)/MNISTNLib.o $(NLIB) $(BASEDIR)/lib/libneuro.a
#	$(CPP) $(CFLAGSBASE) $(CFLAGSNEURO) $(SRC)/TestBatch.cpp $(OBJ)/MNISTNLib.o -o $(BIN)/TestBatch $(LFLAGS)

#$(BIN)/TestNL: $(SRC)/TestNL.cpp $(OBJ)/MNISTNLib.o ../../models/dummy/lib/libneuro.a $(BASEDIR)/lib/libneuro.a
#	$(CPP) $(CFLAGSBASE) $(CFLAGSNEURO) $(SRC)/TestNL.cpp $(OBJ)/MNISTNLib.o -o $(BIN)/TestNL  ../../models/dummy/lib/libneuro.a $(BASEDIR)/lib/libneuro.a

#$(OBJ)/MNISTAE.o: $(SRC)/MNISTAE.cpp $(INC)/MNISTAE.hpp
#	$(CPP) $(CFLAGSBASE) $(SRC)/MNISTAE.cpp -c -o $(OBJ)/MNISTAE.o

$(BIN)/MNISTEO: $(OBJ)/MNISTEO.o $(OBJ)/MNISTNLib.o $(NLIB) $(NEO) $(EOOBJ) $(BASEDIR)/lib/libneuro.a
	$(CPP) $(CFLAGSBASE) $(CFLAGSNEURO) $(OBJ)/MNISTEO.o $(OBJ)/MNISTNLib.o -o $(BIN)/MNISTEO $(EOOBJ) $(LFLAGS) $(NEO)

$(OBJ)/MNISTEO.o: $(SRC)/MNISTEO.cpp $(INC)/MNISTAE.hpp $(INC)/MNISTNLib.hpp
	$(CPP) $(CFLAGSBASE) $(CFLAGSNEURO) $(SRC)/MNISTEO.cpp -c -o $(OBJ)/MNISTEO.o

#$(BIN)/MNISTDEO: $(OBJ)/MNISTEO.o $(OBJ)/MNISTAE.o $(OBJ)/MNISTNLib.o $(NLIB) $(NEO) $(DEOOBJ) $(BASEDIR)/lib/libneuro.a
#	$(MPICC) $(CFLAGSBASE) $(CFLAGSNEURO) $(OBJ)/MNISTEO.o $(OBJ)/MNISTAE.o $(OBJ)/MNISTNLib.o -o $(BIN)/MNISTDEO $(DEOOBJ) $(LFLAGS) $(NEO)

#$(BIN)/User: $(SRC)/User.cpp
#	$(CPP) $(CFLAGSBASE) $(SRC)/User.cpp -o $(BIN)/User
