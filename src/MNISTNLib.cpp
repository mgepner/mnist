//#include "neuro.h"
#include "MNISTNLib.hpp"
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <stdint.h>
#include "utils/params.h"

using namespace std;
using namespace NeuroUtils;

MNISTNLib::MNISTNLib()
{
  neuroNetwork = NULL;
  neuroDevice = NULL;
  neuroInstance = NULL;
  neuroIO = NULL;
  runTime = ParamsGetDouble("mnist", "neuro_run_time", true);
  delete_nd = false;
  delete_nio = false;
  //currentApplyInput = NULL;
  numInputs = 0;
  numOutputs = 0;
}

MNISTNLib::~MNISTNLib(){
    if (delete_nd){
        if (neuroNetwork != NULL) delete neuroNetwork;
        if (neuroDevice != NULL) delete neuroDevice;
    }
    if (neuroInstance != NULL) delete neuroInstance;
    if (delete_nio && neuroIO != NULL) delete neuroIO;
}

void MNISTNLib::InitializeNeuro(const char* network_file){
    if (delete_nd){
        if (neuroNetwork != NULL) delete neuroNetwork;
        if (neuroDevice != NULL) delete neuroDevice;
    }
    neuroNetwork = new NeuroNetwork(network_file, true);
    neuroDevice = new NeuroDevice(neuroNetwork->Config(), "");
    delete_nd = true;
    SetUpInstance();
}

void MNISTNLib::SetUpInstance(){
    if (neuroInstance != NULL) { delete neuroInstance; }
    neuroInstance = new NeuroInstance(neuroNetwork, neuroDevice);

    // Attach current neuroIO to this device
    if (neuroIO != NULL) { neuroIO->SetDevice(neuroDevice); }

}

void MNISTNLib::GameStateToInput(const uint32_t image[28]){

    uint32_t andInt = 1;

    for(int i = 0; i < 28; i++) {
        for(int j=0;j<28;j++){
            if(((image[i] >> j) and andInt) == andInt){
                neuroIO->ApplyScalarInput(ids.numbers[i], 1.0, j*5);
            }
        }
    }
    //return 1;
}

int MNISTNLib::OutputToGameInput()
{
    int mostFires = 0;
    int holder;
    int move;
    neuroInstance->Simulate(runTime);

    for(int num=0;num<2;num++){
        holder = neuroIO->GetCountOutput(num);
        if(holder > mostFires){
            mostFires = holder;
            move = num;
        }
    }

    if(mostFires == 0){return -1;}
    
    return move;
}

void MNISTNLib::SetEONeuroIO() {

	int power = 1;
    int nbins = 1;
    int pulses_per_bin = 2;
    int pulse_time_difference = 5;


    // Delete existing neuroIO setup, if necessary
    if (delete_nio && neuroIO != NULL) delete neuroIO;
    delete_nio = true;
    neuroIO = new NeuroIO;

    // Use NeuroIO file if specified
    string neuroIO_file = ParamsGetString("mnist", "eo_neuroIO_file", false);
    if (neuroIO_file.size() > 0) {
        InitializeIO(neuroIO_file.c_str());
        return;
    }

    ids.numbers.clear();
    for(int i=0;i<28;i++){
        ids.numbers.push_back(neuroIO->AddScalarInput(1, 2, power, power, nbins, pulses_per_bin, pulse_time_difference, "SIMPLE"));
    }

    ids.output.push_back(neuroIO->AddCountOutput(0));
    ids.output.push_back(neuroIO->AddCountOutput(0));

/*    
    for(int i=0;i<10;i++){
        ids.output.push_back(neuroIO->AddCountOutput(0));
    }
*/
    numInputs = neuroIO->TotalNetworkInputs();
    numOutputs = neuroIO->TotalNetworkOutputs();

}

void MNISTNLib::SetUpIO() {
    if (neuroIO == NULL) {
        fprintf(stderr, "MNISTNLib::SetUpIO error: neuroIO has not been created.\n");
        exit(1);
    }
    if (neuroDevice == NULL) {
        fprintf(stderr, "MNISTNLib::SetUpIO error: neuroDevice has not been created.\n");
        exit(1);
    }

    // Attach current neuroIO to this device
    neuroIO->SetDevice(neuroDevice);
}

void MNISTNLib::SetUpIOIds(){
    if (neuroIO == NULL) {
        fprintf(stderr, "MNISTNLib::SetUpIOIds error: neuroIO has not been created.\n");
        exit(1);
    }
    // Special ApplyInput functions don't use the IO ids
    //if (currentApplyInput != NULL) return;

    // Set up the ids struct based on neuroIO
    numInputs = neuroIO->TotalNetworkInputs();
    numOutputs = neuroIO->TotalNetworkOutputs();

}

void MNISTNLib::InitializeIO(const char* neuroIO_file) {
    //int desiredInputs;
    // Delete existing neuroIO setup, if necessary
    if (delete_nio && neuroIO != NULL) delete neuroIO;
    delete_nio = true;

    if (neuroIO_file == NULL || neuroIO_file[0] == '\0') {
        numInputs = neuroIO->TotalNetworkInputs();
        numOutputs = neuroIO->TotalNetworkOutputs();
    }
    else {
        neuroIO = new NeuroIO;
        if (!neuroIO->Deserialize(neuroIO_file, true)) {
            fprintf(stderr, "MNISTNLib::InitializeIO: Deserializing given NeuroIO file failed: %s\n", neuroIO_file);
            exit(1);
        }
        SetUpIOIds();
    }

    // Attach this neuroIO to current device
    if (neuroDevice != NULL) neuroIO->SetDevice(neuroDevice);
}

string MNISTIOIds::Serialize() {
    stringstream ss;
    ss << "# MNISTIOIds" << endl;
    //ss << "input " << input << endl;
    ss << "output " << output.size() << endl;
    return ss.str();
}

bool GetTestCasesFromFile(const char* file, vector<vector<int> > &test_cases){
    ifstream fin;
    string line;
    int number;
    int imageCounter = 0;
    //const char* line;
    vector<int> zero, one, two, three, four, five, six, seven, eight, nine;

    fin.open(file, ifstream::in);
    if (!fin.is_open()) {
        fprintf(stderr, "Error opening test cases file %s\n", file);
        perror("");
        exit(1);
    }

    while (getline(fin,line)) {
        istringstream tmp(line);
        tmp >> number;

        switch(number){
            case 0: zero.push_back(imageCounter); break;
            case 1: one.push_back(imageCounter); break;
            case 2: two.push_back(imageCounter); break;
            case 3: three.push_back(imageCounter); break;
            case 4: four.push_back(imageCounter); break;
            case 5: five.push_back(imageCounter); break;
            case 6: six.push_back(imageCounter); break;
            case 7: seven.push_back(imageCounter); break;
            case 8: eight.push_back(imageCounter); break;
            case 9: nine.push_back(imageCounter); break;
        }

        imageCounter++;
        //test_cases.push_back(number);
    }
    test_cases.push_back(zero);
    test_cases.push_back(one);
    test_cases.push_back(two);
    test_cases.push_back(three);
    test_cases.push_back(four);
    test_cases.push_back(five);
    test_cases.push_back(six);
    test_cases.push_back(seven);
    test_cases.push_back(eight);
    test_cases.push_back(nine);

    return true;
}