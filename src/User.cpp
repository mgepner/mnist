// This program is a prototype for user accessing the
// neural network appication.


#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <cstring>
using namespace std;


int main(int argc, char** argv){
    int sockfd;
    int portno;
    int n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];
    
    // Access the arguments from the command line.
    if(argc < 2){
        fprintf(stderr, "ERROR: incorrect command line arguments provided.\n");
        exit(1);
    } else{
        server = gethostbyname(argv[1]);
        portno = atoi(argv[2]);
    }
    
    // Connect to the socket.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0){
        fprintf(stderr, "ERROR: failed to open socket.\n");
        exit(1);
    }
    
    // Check the host name.
    if(server == NULL){
        fprintf(stderr, "ERROR: no such host.\n");
        exit(1);
    }
    
    // Sets all values in the specified buffer to 0.
    memset((char *) &serv_addr, 0, sizeof(serv_addr));
    
    // Just do this.
    serv_addr.sin_family = AF_INET;
    
    //
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr, server->h_length);
    
    //
    serv_addr.sin_port = htons(portno);
    if(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        fprintf(stderr, "ERROR: failed to connect to the given socket.");
    }
    
    // Temporarily here...
    int i, sn, last_ans;
    char game_state, buf[500];
    
    // Now, perform communication to the application.
    while(1){
        // Obtain the state message back.
        memset(buffer, 0, 256);
        n = read(sockfd, buffer, 255);
        if(n < 0){
            fprintf(stderr, "ERROR: failed to read message from the socket.\n");
            close(sockfd);
            exit(1);
        }
        
        // Access the different elements of the game's state.
        i = sscanf(buffer, "SN: %d  LA: %d  Result: %c  Game: %s",
                   &sn,
                   &last_ans,
                   &game_state,
                   (char *) &buf);
        if(i != 4){
            i = sscanf(buffer, "Game Over - Winner: %c", &game_state);
            if(i != 1){
                fprintf(stderr, "User ERROR: GetState() wrong format: %s\n", buffer);
                exit(1);
            } else {
                if(game_state == 'X') printf("Game Over - You Win!\n");
                else printf("Game Over - You Lose.\n");
                exit(0);
            }
        }
        
        printf("%s\n", buffer);
        
        // If it's the player's turn, send a message back.
        if(last_ans == 'O'){
            printf("Enter a move: 0-8\n");
            memset(buffer, 0, 256);
            fgets(buffer, 255, stdin);
            n = write(sockfd, buffer, strlen(buffer));
            if(n < 0){
                fprintf(stderr, "ERROR: failed to write message to socket buffer.\n");
                close(sockfd);
                exit(1);
            }
        }
    }
    
}










































