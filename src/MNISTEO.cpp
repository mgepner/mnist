#include "neuro.h"
#include "MNISTAE.hpp"
#include "MNISTNLib.hpp"
#include "utils/params.h"
#include "utils/ThreadSafePool.h"
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdint.h>
#include <fstream>

using namespace std;
using namespace NeuroUtils;

//string Params;
//static int Nodes;

/*
	MNISTApp made to mimic PoleBalanceApp,
	in case there are things to be added later
*/

class MNISTApp{
	public:
		string Params;
        int Threshold;
        int label;// will be set in the mnist.txt and determines what number the network is learning to distinguish
		vector<vector<int> > testCases;
        //ThreadSafePool<MNISTAE*> aePool;
        ThreadSafePool<MNISTNLib*> nlibPool;
		int num_inputs;
		int num_outputs;
		MNISTApp(const string &testCasesFile);
};

MNISTApp::MNISTApp(const string &testCasesFile){
	GetTestCasesFromFile(testCasesFile.c_str(), testCases);
    Threshold = ParamsGetInt("mnist", "eo_test_cases_threshold", true);
    printf("Loaded %ld test cases.\n", testCases.size());
}

static MNISTApp *ma = NULL;


void AppEOInit(){

	ma = new MNISTApp(ParamsGetString("mnist", "eo_test_case_labels", true));

    ma->label = ParamsGetInt("mnist", "eo_mnist_label", true);

	MNISTNLib* mnistn = new MNISTNLib;
	mnistn->SetEONeuroIO();

	ma->nlibPool.CheckIn(mnistn);

	mnistn = ma->nlibPool.CheckOut(true);

	ma->num_inputs = mnistn->numInputs;
	ma->num_outputs = mnistn->numOutputs;

    string nio_string = mnistn->neuroIO->Serialize();
    ofstream fout("neuroio.txt");
    if (!fout.is_open()) {
        fprintf(stderr, "Error: couldn't open neuroio.txt for writing.\n");
        exit(1);
    }
    fout << nio_string;
    fout.close();
    printf("Using %d inputs and %d outputs\n", ma->num_inputs, ma->num_outputs);
    cout << mnistn->ids.Serialize() << endl << nio_string << endl;

	ma->nlibPool.CheckIn(mnistn);

}

void SetAppConfiguration(AppConfig &appconfig){
	int elements = ParamsGetInt("mnist", "eo_num_elements", true);
    appconfig.deviceConfig = NeuroGetDeviceConfig("Default", elements, ma->num_inputs, ma->num_outputs);
    appconfig.nInputs = ma->num_inputs;
    appconfig.nOutputs = ma->num_outputs;
}

double Fitness(NeuroNetwork *network, NeuroDevice *device){

	MNISTNLib *mnistn;
    //MNISTAE* ae; //the engine
    uint32_t image[28] = {0};
    uint32_t shiftValue = 1;
    int ans, pix;
    double number_correct = 0;
    double fitness_value;
    ifstream fin;
    char fn[10000];
    string temp;
    int numberCheck;

    mnistn = ma->nlibPool.CheckOut(true);

   	mnistn->neuroDevice = device;
    mnistn->neuroNetwork = network;
    mnistn->SetUpInstance();
    mnistn->SetUpIO();


    for(int i=0;i<10;i++){
        numberCheck = 0;
        for(int j=0;j<10;j++){

        mnistn->neuroInstance->StartJob();

        sprintf(fn, "tests/test_img_%04d.pgm", ma->testCases[i][j]);
        fin.open(fn, ifstream::in);
        if (fin.fail()) {
            cerr << "Failed to open " << fn << endl;
            exit(1);
        }
        fin >> temp >> temp >> temp >> temp;
        for(int j=0;j<28;j++){
            for(int k = 0; k < 28; k++){
                fin >> pix;
                if((255-pix) > ma->Threshold){
                    image[k] = (image[k] | (shiftValue << j));
                }
                else{}
            }
        }
        fin.close();

        mnistn->GameStateToInput(image);
        ans = mnistn->OutputToGameInput();

        if(ans == -1){ break; }

        if((ma->label == i) && (ans == 0)){
            number_correct += 1;
            numberCheck++;

        }
        else if((ma->label != i) && (ans == 1)){
            number_correct += 1;
            numberCheck++;
        }

        mnistn->neuroInstance->StopJob('A');

        }
        if(numberCheck != 10){ break; }

    }

    if(numberCheck != 10){number_correct = 0;} //network couldnt get the initial ten digits
    else{ 
        number_correct = 0;
        for (int i = 0; i < 10; i++) {
        
            for(int j=0;j<40;j++){

    	       mnistn->neuroInstance->StartJob();  
        
                sprintf(fn, "tests/test_img_%04d.pgm", ma->testCases[i][j]);
                fin.open(fn, ifstream::in);
                if (fin.fail()) {
                    cerr << "Failed to open " << fn << endl;
                    exit(1);
                }
                fin >> temp >> temp >> temp >> temp;
                for(int j=0;j<28;j++){
                    for(int k = 0; k < 28; k++){
                        fin >> pix;
                        if((255-pix) > ma->Threshold){
                            image[k] = (image[k] | (shiftValue << j));
                        }
                        else{}
                    }
                }
                fin.close();

    	        mnistn->GameStateToInput(image);
        	    ans = mnistn->OutputToGameInput();

                mnistn->neuroInstance->StopJob('A');

                if(ans == -1){ break; }

                if((ma->label == i) && (ans == 0)){
                    number_correct += 1;
                }
                else if((ma->label != i) && (ans == 1)){
                    number_correct += 1;
                }

            }
        }

    }

    ma->nlibPool.CheckIn(mnistn);

  	//delete mnistn;

  	fitness_value = (number_correct / 400.00);

  	return fitness_value;
}