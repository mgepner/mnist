//  Ty Vaughan, Miles Gepner, Stephen Kwan
//  Senior Design 402
//  Feb. 9 2017
//
//  Program Description:
//  This program takes input as follows:
//  SEED: the seed for random number generation
//  Gameboard: A string of nine dashes/X's/O's representing the original
//      state of the game board.
//  CHAR_FIRST: a character representing who will go first.  If it is an
//      X, then the player will go first.  If it is an O, then the net
//      will go first. Player will always be X.

#include "MNISTAE.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
using namespace std;

/* Broadcast output to all of the outputs. */
/* Credit: Dr. Jim Plank */
static void write_all(const string s, vector <FILE *> &outputs){
    int i;
    
    for(i = 0; i < (int) outputs.size(); i++){
        fprintf(outputs[i], "%s", s.c_str());
        fflush(outputs[i]);
    }
}

/* The main Application Player */
int main(int argc, char **argv){
    long seed;
    string board;
    string state;
    string turn;
    //FILE *o1, *i1;
    //vector <FILE *> outputs;
    MNISTAE ae;
    
    int sn, i, last_ans, choice;
    char game_state;
    
    int sockfd, sockfd_in1, sockfd_in2;
    int portno = 5555;
    socklen_t userlen;
    char buffer[256];
    struct sockaddr_in serv_addr, user_addr;
    int n;
    
    /* Parse the command line */
    if(argc != 4){
        //write_all("Input should be in the format: SEED ######### CHAR_FIRST\n", outputs);
        exit(0);
    } else {
        seed = atoi(argv[1]);
        board = argv[2];
        turn = (strcmp(argv[3],"X")==0) ? "1" : "0";
        printf("Seed: %ld  Board: %s  Turn: %s\n", seed, board.c_str(), turn.c_str());
    }
    
    
    // ####################################
    //   Set up the I/O connections
    // ####################################
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0){
        fprintf(stderr, "ERROR: could not open the socket.\n");
        exit(1);
    }
    
    // Set up the server sock on the desired port..
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        fprintf(stderr, "ERROR: could not bind to port.\n");
        exit(1);
    }
    
    // Only have a maximum of 5 connections waiting at a time.
    listen(sockfd,5);
    
    // Connect to both the player/viz and the neural net.
    userlen = sizeof(user_addr);
    sockfd_in1 = accept(sockfd, (struct sockaddr *) &user_addr, &userlen);
    if(sockfd_in1 < 0){
        fprintf(stderr, "ERROR: could not accept incoming connection.\n");
        exit(1);
    }
    /*
    //sockfd_in2 = accept(sockfd, (struct sockaddr *) &user_addr, &userlen);
    //if(sockfd_in2 < 0){
    //    fprintf(stderr, "ERROR: could not accept incoming connection.\n");
    //    exit(1);
    //}
    
    // Loop to be commented out.  User for learning.
    while(1){
        // Get message from the player.
        bzero(buffer,256);
        n = read(sockfd_in1, buffer, 255);
        if(n < 0){
            fprintf(stderr, "ERROR: could not read message from the player.\n");
            close(sockfd_in1); // close player before server.
            close(sockfd);
            exit(1);
        }
        printf("Player Message: %s\n", buffer);
        
        // Send message to the player.
        bzero(buffer,256);
        n = write(sockfd_in1, "-- fake game update --", 22);
        if(n < 0){
            fprintf(stderr, "ERROR: could not write a message to the player.\n");
            close(sockfd_in1); // close player before server.
            close(sockfd);
            exit(1);
        }
    }*/
    
    // ####################################
    //   Finish setting up the connections.
    // ####################################
    
    /* Seed the random number generator for our temporary, fake neural net response. */
    srand(seed);
    
    /* Begin new run. */
    ae.NewGame(turn + " " + board);
    
    /* Now, go into the main loop. */
    char buf[500]; // <- temporary cause it's late and I'm sleepy.
    while(1){
        state = ae.GetState();
        cout << state << endl;
        
        // Access the different elements of the game's state.
        i = sscanf(state.c_str(), "SN: %d  LA: %d  Result: %c  Game: %s",
            &sn,
            &last_ans,
            &game_state,
            (char *) &buf);
        board = buf;
        if(i != 4){
            fprintf(stderr, "MNISTAP ERROR: GetState() wrong format: %s\n", state.c_str());
            exit(1);
        }
        
        // If the game is over, send the ending message.
        if(game_state != 'n'){
            printf("MNISTAP: Game Over! %c is the winner!\n",game_state);
            for(i = 0; i < 9; i+= 3){
                printf("%c  %c  %c\n",board[i],board[i+1],board[i+2]);
            }
            
            // Send the state of the game to the player.
            bzero(buffer,256);
            sprintf(buffer, "Game Over - Winner: %c", game_state);
            n = write(sockfd_in1, buffer, 22);
            if(n < 0){
                fprintf(stderr, "ERROR: could not write a message to the player.\n");
                close(sockfd_in1); // close player before server.
                close(sockfd);
                exit(1);
            }
            
            exit(0);
        }
        
        // Send the state of the game to the player.
        bzero(buffer,256);
        n = write(sockfd_in1, state.c_str(), state.size());
        if(n < 0){
            fprintf(stderr, "ERROR: could not write a message to the player.\n");
            close(sockfd_in1); // close player before server.
            close(sockfd);
            exit(1);
        }
        
        cout << "turn: " << turn << "_sn: " << sn << endl;
        // Obtain the next response from the proper player.
        if(last_ans == 'O'){
           //|| (turn == "1" && sn == 0)){
            
            // Get a response the player.
            bzero(buffer,256);
            n = read(sockfd_in1, buffer, 255);
            if(n < 0){
                fprintf(stderr, "MNISTAP ERROR: could not read message from the player.\n");
                exit(1);
            }
            i = sscanf(buffer, "%d", &choice);
            if(i != 1){
                fprintf(stderr, "MNISTAP ERROR: player input incorrect format.\n");
            } else {
                printf("User move: %d\n", choice);
            }
        } else {
            // Obtain the neural net's response (or generate a fake response - wooo!)
            choice = rand() % 9;
            while(board[choice] != '-'){
                choice = rand() % 9;
            }
        }
        
        // Update the game's state with the received response.
        printf("Update the State with: %d\n", choice);
        ae.UpdateState(choice);
    }
    
    
    /* ... */
    
}
