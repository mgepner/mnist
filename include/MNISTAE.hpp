#include <string>
#include <vector>


/* ------------------------------------------------------------
   NewRun() starts a new run.  Params is a string of "KEY VAL\n" strings.  

      All of these can be set in .xorrc if you want, and if you don't specify them, 
      I'll have defaults:

      "W w", which says the number of bits in the words.  The default is 1.
      "SEED s" -- RNG seed.  Default = -1 = time(0)
      "INPUTS s" -- Instead of randomly generating the numbers, have them specified by this
         string as space separated hex.  
 
   GetState(): This returns a string in the following format:
      "%c %d %d %s %s": Last-correct (y|n|-), Correct so far, incorrect so far, next-a, next-b.

       Next-a and Next-b are the inputs to the next problem.  
       They are given in binary (e.g. "0101").

   UpdateState(): The input is simply "%s", which is the XOR of next-a and next-b.
*/

using namespace std;

class MNISTAE{
    public:
        MNISTAE();
        ~MNISTAE();
        void NewGame(const string &params);
        void NewGameTestCase(const string &params);
        string GetState();
        void UpdateState(const int &inputs);
    protected:
        void* state;
};
