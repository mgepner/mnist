#include <fstream>
#include <cstring>
#include <cstdlib>
#include <stdint.h>
#include "neuro.h"
//#include "neuroparam.h"
#include "neuroio.h"

using namespace std;

class MNISTIOIds {
    public:
        //int bias;
        vector<int> output;
        string Serialize();
        vector<int> numbers;
};

class MNISTNLib {
    public:
        MNISTNLib();
        ~MNISTNLib();

        /* InitializeNeuro(): 
           You call this if you are reading your network from a file. 
           It reads the network, allocates an instance of the device,
           and sets up the NeuroInstance, so that it's ready to run. 
           This calls SetupInstance(). */

        void InitializeNeuro(const char* network_file);

        /* SetupInstance(), on the other hand, can either be called from 
           InitializeNeuro() above, or it can be called if you have
           already set up the network and device in the neuroNetwork and neuroDevice
           variables.  It will do some error checking to make sure that the 
           network has enough inputs and outputs, and it will create the instance,
           which loads the network onto the device.  It also sets up the outputs.
           When this is done, the device is ready to run (i.e. call StartJob(), and
           then ApplyInputs(), etc. */

        void SetUpInstance();

        /* GameStateToInput() takes a string that represents the game state (this
           comes from the Game Engine).  It parses the string, and returns 0 if
           the game is over.  Otherwise, it creates the proper ApplyInput events
           and sends them to the device.  It then returns 1. */

        void GameStateToInput(const uint32_t image[28]);

        /* InitializeIO():
            This sets up the NeuroIO object that will be used. If given a file
            name, it will use that to construct the NeuroIO object. Otherwise, 
            if the neuroIO member variable has not been set yet, it will be 
            created using a best-guess based on the currently-loaded network. 
            If no network has been loaded, it will use some default configuration
            that you won't like. 
            
            This sets the neuroIO to the current device, if there is a current device.
        */

        void InitializeIO(const char* neuroIO_file);

        /* OutputToGameInput() calls Simulate() on the Instance, and reads the output.
           From that, it passes an int that represents the move on the game board as 
           input to the game. */

        int OutputToGameInput();

         /* EO uses this to configure the IO. It uses the IO configuration in the
            params file if specified. Otherwise, it crafts a neuroIO on its own.
        */
        void SetEONeuroIO();

        /* ApplyInput():
            This uses the PBState member variable to apply the appropriate input
            fires to the device. */
        void ApplyInput();

        /* GetDecision() calls ApplyInput() with the given state, runs the 
            device for the given amount of time, and returns ReadOutput(). */
        //int GetDecision(const char* state, double neuroRunTime);

        /* SetUpIO():
            This must be called to attach neuroIO to the current device if the
            neuroIO member variable was set manually without using InitializeIO,
            or if the neuroDevice member was changed after InitializeIO was called. 
        */
        void SetUpIO();

        /* SetUpIOIds():
            This sets up the input and output IDs that will be used with the neuroIO 
            object. This must be called if the neuroIO member was set manually. 
        */
        void SetUpIOIds();

        NeuroInstance *neuroInstance;
        NeuroNetwork  *neuroNetwork;
        NeuroDevice   *neuroDevice;
        NeuroIO		  *neuroIO;
        //int W;
        int numInputs;
        int numOutputs;
        double runTime;
        int delete_nd;  /* This will be true if you want 
                           to delete the network & device when you destruct. */
        int delete_nio;

        MNISTIOIds ids;
};

bool GetTestCasesFromFile(const char* file, vector<vector<int> > &test_cases);
